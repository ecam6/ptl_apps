# Pick to light

Projet réalisé au sein du mastère ETNO à l'ECAM Strasbourg année 2020-2021.

Le but du projet est de réaliser un pick to light sur la ligne en U de l'ecam. Nous devons nous baser sur le MES puisque c'est lui qui pilote la ligne. Chaque opérateur doit appuyer sur suivant sur la tablette lorsqu'une tâche est terminé. Ce process n'est pas parfait mais ne demande qu'à être améliorer

Comme nous avons l'information dans le MES de l'état exact de la ligne de production il est inutile d'inventer un autre système, il suffit de récuperer les informations.

Afin de rendre ce projet souple il a été pensé de telle manière à pouvoir ajouter n'importe quel type de données en entrée et pouvoir actionner n'importe quel type de lumière en sortie.

Actuellement a été développé :

**En entrée :**
- Aquiweb
- Simulator

**En sortie :**
- Iolink (IFM)
- ESPHome

Ce projet est entièrement basé sur Python et est construit en micro-service.

# Architecture

![Picture of architecture](./architecture.svg)

Liste des services et technologies :

| Service/Projets                                       | Technologie                                                     | Port (en production)                                      | Remarque                                                                          |
| ----------------------------------------------------- | --------------------------------------------------------------- | --------------------------------------------------------- | --------------------------------------------------------------------------------- |
| [ptl_apps](https://gitlab.com/ecam6/ptl_apps)         | --------------------------------------------------------------- | --------------------------------------------------------- | Projet principal
| [provisioning](https://gitlab.com/ecam6/provisioning) | [Ansible](https://www.ansible.com/)                             | --------------------------------------------------------- |
| portainer                                             | [Portainer](https://www.portainer.io/)                          | 9000                                                      | Only in production                                                                |
| [front](https://gitlab.com/ecam6/front)               | [Django](https://www.djangoproject.com/)                        | 80                                                        | Cette partie peut être améliorer en passant sur une techno comme angular ou vuejs |
| [api](https://gitlab.com/ecam6/api)                   | [Django Rest Framework](https://www.django-rest-framework.org/) | 8600                                                      |
| broker                                                | [RabbitMQ](https://www.rabbitmq.com/) (AMQP, MQTT)              | 8080 (UI), 5672 (default), 15675 (WebMQTT), 1883 (MQTT)   |
| database                                              | [PostgreSQL](https://www.postgresql.org/)                       |                                                           |
| [gateway](https://gitlab.com/ecam6/gateway)           | [Python](https://www.python.org/)                               |                                                           |
| [mapper](https://gitlab.com/ecam6/mapper)             | [Python](https://www.python.org/)                               |                                                           |
| [provider](https://gitlab.com/ecam6/provider)         | [Python](https://www.python.org/)                               |                                                           | Utilisé en mode production, désactivé en mode démonstration                       |
| [simulator](https://gitlab.com/ecam6/simulator)       | [Python](https://www.python.org/)                               |                                                           | Utilisé en mode démonstration, désactivé en mode production                       |
| [ESP](https://gitlab.com/ecam6/esp)                   | [ESPHome](https://esphome.io/)                                  | --------------------------------------------------------- |


# Configuration

1. Configuration des postes

    Vous pouvez configurer les postes de 3 manières :
    - Aller sur l'interface Web dans l'onglet Settings>Workstations puis cliquez sur synchroniser les données MES. Dans ce cas l'application va chercher la configuration du MES et la synchroniser avec l'actuelle
    - Ajouter les workstations à la main
    - Utiliser la [base de données par défault](#Database-management)

2. Configuration des bacs

    La configuration des bacs ne peut pas encore être synchroniser depuis le MES (__amélioration future ?__), vous avez 2 choix :
    - Ajouter les bacs ainsi que les articles associé à la main
    - Utiliser la [base de données par défault](#Database-management)

3. Configuration des sorties

    **ESPHome**

    Pour la partie ESP il n'y a aucune configuration à faire. Par défault le logiciel écoutera sur le serveur MQTT à la recherche d'ESP.

    L'assignation de l'ESP à un poste se fait via son nom. (voir [ESP](https://gitlab.com/ecam6/esp))

    **IOLink (IFM)**

    La configuration se fait au niveau de la workstation.

4. Configuration générale

    Vous pouvez régler différents paramètres :
    - La couleur des LEDs, une seule couleur pour toute la ligne (__amélioration future ?__)
    - La luminosité des LEDs
    - Le mode de l'application
        * OFF : L'application est éteinte
        * DEMO : L'application est en mode démonstration (provider OFF, simulator ON)
        * PROD : L'application est en mode production (provider ON, simulator OFF)

# Installation

## Requirements

- Docker
- Docker compose
- Docker started in Swarm mode

!! Une connection internet est obligatoire pour l'installation !!

Vous pouvez trouver un [script d'installation](https://gitlab.com/ecam6/provisioning) qui vous guidera pour installer le logiciel sur un serveur

## Production

Suivez le [script d'installation](https://gitlab.com/ecam6/provisioning) pour toute installation en production !

## Development

Dans une première fenêtre et depuis le repertoire courant executé :
```shell
make run_dev
```

Une fois l'application lancén, dans une seconde fenêtre nous allons initialiser la base de données :

`make migrate_dev && make seed_dev`

## Database management

Il existe une base de données par défault qui contient la configurations des postes, des bacs et des articles pour la ligne de production.

Lorsque vous lancer le [script d'installation](https://gitlab.com/ecam6/provisioning) cette base de données sera automatiquement créer et rempli avec les données par défault.

Vous pouvez déployer cette base de données en développement

__Remarque__: Lorsque cette base est déployé l'application est en mode OFF par défaut.

# Maintenance

Portainer est déployé en production permettant de facilité les interventions de maintenances.

Lorsqu'un problème survient :
1. Comparer l'état logiciel des LEDs avec l'état réel (Si l'état logiciel semble correct mais pas l'état physique, privilégié un problème matériel sinon continuer)
2. Vérifier que toutes les services sont lancées. (Portainer) => relancer le service le cas échéant
3. Vérifier qu'aucune queue ne se remplisse. (RabbitMQ UI) => Suivant la queue qui se rempli reprennez le schéma de l'architecture vous devriez être capable d'identifier le service qui pose problème
4. Vérifier tout le flux, ouvrez les logs de chacun des process et suivez le chemin de la données. (Portainer)

Solution ultime:
1. Supprimer la stack / Formater le serveur (:D)
2. Relancer le [script d'installation](https://gitlab.com/ecam6/provisioning)

# Important !

**Remarque sur la sécurité de cette application**

Cette application n'est **PAS** sécurisée. Elle a été développé dans le cadre d'un projet étudiant où le sujet principal n'est pas la sécurité.

- Ce manque de sécurité n'impact qu'elle même. Il n'y a pas d'effet de bord sur d'autres systèmes.

- Aucune authentification n'a été mis en place, ni pour l'accès à l'interface web ni pour l'accès à l'API ni pour l'accès au MQTT.

- Toutes les communications sont en clair (HTTP, MQTT, AMQP...).

- Pour des raisons de debug et de maintenance les applications sont mis en production en mode DEBUG ! Si cette application venait à être utilisé tous les jours et donc dans une vraie production il est **OBLIGATOIRE** de renforcer la sécurité et de déployer en production.
