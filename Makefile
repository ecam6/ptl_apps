run:
	docker-compose -f docker-compose.yml up --build
run_dev:
	docker-compose -f docker-compose.dev.yml up --build
migrate_dev:
	docker-compose -f docker-compose.dev.yml exec api python3 manage.py migrate
seed_dev:
	docker-compose -f docker-compose.dev.yml exec api python3 manage.py loaddata seed.json
update_api:
	docker-compose -f docker-compose.dev.yml exec front swagger_codegen generate http://api:8000/api/schema ptl_api
	docker-compose -f docker-compose.dev.yml exec mapper swagger_codegen generate http://api:8000/api/schema ptl_api
	docker-compose -f docker-compose.dev.yml exec provider swagger_codegen generate http://api:8000/api/schema ptl_api
	docker-compose -f docker-compose.dev.yml exec simulator swagger_codegen generate http://api:8000/api/schema ptl_api
